<?php
/*
 * Generated by CRUDigniter v3.2
 * www.crudigniter.com
 */

class Config extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Config_model');
    }

    /*
     * Listing of config
     */
    function index()
    {
        $data['config'] = $this->Config_model->get_all_config();

        $data['_view'] = 'config/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new config
     */
    function add()
    {
      $this->load->library('form_validation');
  		$this->form_validation->set_rules('foreman_login','Foreman Login','max_length[45]|required');
  		$this->form_validation->set_rules('foreman_password','Foreman Password','max_length[45]|required');
  		$this->form_validation->set_rules('foreman_url','Foreman Url','max_length[45]|required');
  		$this->form_validation->set_rules('foreman_var_namespaces','Foreman Var Namespaces','max_length[45]|required');
  		$this->form_validation->set_rules('foreman_var_volumes','Foreman Var Volumes','max_length[45]|required');
      $this->form_validation->set_rules('foreman_var_policy','Foreman Var Policy','max_length[45]|required');
  		$this->form_validation->set_rules('name','Name','max_length[45]|is_unique[config.name]');
  		$this->form_validation->set_rules('kubernetes_master_fqdn','Kubernetes Master Fqdn');
  		$this->form_validation->set_rules('is_default','Is Default');

  		if($this->form_validation->run())
      {
        $params = array(
  				'is_default' => $this->input->post('is_default'),
  				'kubernetes_master_fqdn' => $this->input->post('kubernetes_master_fqdn'),
  				'foreman_login' => $this->input->post('foreman_login'),
          'foreman_password' => $this->input->post('foreman_password'),
  				'foreman_url' => $this->input->post('foreman_url'),
  				'foreman_var_namespaces' => $this->input->post('foreman_var_namespaces'),
  				'foreman_var_volumes' => $this->input->post('foreman_var_volumes'),
          'foreman_var_policy' => $this->input->post('foreman_var_policy'),
  				'name' => $this->input->post('name'),
          'policy_custom' => $this->input->post('policy_custom'),
        );

        $config_id = $this->Config_model->add_config($params);
        redirect('config/index');
      }
      else
      {
        $data['_view'] = 'config/add';
        $this->load->view('layouts/main',$data);
      }
    }

    /*
     * Editing a config
     */
    function edit($id)
    {
        // check if the config exists before trying to edit it
        $data['config'] = $this->Config_model->get_config($id);

        if(isset($data['config']['id']))
        {
          $this->load->library('form_validation');
    			$this->form_validation->set_rules('foreman_login','Foreman Login','max_length[45]|required');
    			$this->form_validation->set_rules('foreman_password','Foreman Password','max_length[45]|required');
    			$this->form_validation->set_rules('foreman_url','Foreman Url','max_length[45]|required');
    			$this->form_validation->set_rules('foreman_var_namespaces','Foreman Var Namespaces','max_length[45]|required');
    			$this->form_validation->set_rules('foreman_var_volumes','Foreman Var Volumes','max_length[45]|required');
          $this->form_validation->set_rules('foreman_var_policy','Foreman Var Policy','max_length[45]|required');
    			$this->form_validation->set_rules('name','Name','max_length[45]');
    			$this->form_validation->set_rules('kubernetes_master_fqdn','Kubernetes Master Fqdn');
    			$this->form_validation->set_rules('is_default','Is Default');

    			if($this->form_validation->run())
          {
            $params = array(
      				'is_default' => $this->input->post('is_default'),
      				'kubernetes_master_fqdn' => $this->input->post('kubernetes_master_fqdn'),
      				'foreman_login' => $this->input->post('foreman_login'),
      				'foreman_password' => $this->input->post('foreman_password'),
      				'foreman_url' => $this->input->post('foreman_url'),
      				'foreman_var_namespaces' => $this->input->post('foreman_var_namespaces'),
      				'foreman_var_volumes' => $this->input->post('foreman_var_volumes'),
              'foreman_var_policy' => $this->input->post('foreman_var_policy'),
      				'name' => $this->input->post('name'),
      				'policy_custom' => $this->input->post('policy_custom'),
            );

            if($params['is_default'] == TRUE) {
              $default_config = $this->Config_model->get_default_config();
              if($default_config['id'] != $id)
              {
                $default_config['is_default']=FALSE;
                $this->Config_model->update_config($default_config['id'],$default_config);
              }
              // show_error('Default config ID:'.$default_config['id'].' - This config id: '.$id);
            }
            $this->Config_model->update_config($id,$params);
            redirect('config/index');
          }
          else
          {
            $data['_view'] = 'config/edit';
            $this->load->view('layouts/main',$data);
          }
        }
        else
            show_error('The config you are trying to edit does not exist.');
    }

    /*
     * Editing a config
     */
    function default($id)
    {
      $config = $this->Config_model->get_config($id);
      $default_config = $this->Config_model->get_default_config();
      if($default_config['id'] != $id)
      {
        $default_config['is_default']=FALSE;
        $config['is_default']=TRUE;
        $this->Config_model->update_config($default_config['id'],$default_config);
        $this->Config_model->update_config($id,$config);
      }
      redirect('config/index');
    }

    /*
     * Deleting config
     */
    function remove($id)
    {
        $config = $this->Config_model->get_config($id);

        // check if the config exists before trying to delete it
        if(isset($config['id']))
        {
            $this->Config_model->delete_config($id);
            redirect('config/index');
        }
        else
            show_error('The config you are trying to delete does not exist.');
    }

    function sync($object = "",$config_id = "1")
    {
      $config = $this->Config_model->get_default_config();
      $foreman_connect = array('url' => $config['foreman_url'], 'username' => $config['foreman_login'], 'password' => $config['foreman_password']);
      $this->load->library('Foreman',$foreman_connect);

      $content = "";
      if($object == "namespaces")
      {
        $this->load->model('Knamespace_model');

        $smartclass = $this->foreman->read_smartclass($config['foreman_var_namespaces']);

        if(isset($smartclass['id']))
        {
          $content = array();
          $value = array();
          foreach($smartclass['override_values'] as $override)
          {
            if($override['match'] == "fqdn=".$config['kubernetes_master_fqdn'])
            {
              $namespaces = $this->Knamespace_model->get_all_knamespaces();
              foreach ($namespaces as $namespace) {
                $id= [ 'id' => "$namespace[id]"];
                unset($namespace['id']);
                if($namespace['quota_hard_limits_cpu'] != 0 && $namespace['quota_hard_limits_cpu'] % 1000 == 0) {
                  $namespace['quota_hard_limits_cpu'] /= 1000;
                } else {
                  $namespace['quota_hard_limits_cpu'] .="m";
                }
                if($namespace['quota_hard_limits_memory'] != 0 && $namespace['quota_hard_limits_memory'] % 1024 == 0){
                  $namespace['quota_hard_limits_memory'] /= 1024;
                  $namespace['quota_hard_limits_memory'] .= "Gi";
                } else {
                  $namespace['quota_hard_limits_memory'] .= "Mi";
                }
                if($namespace['quota_hard_cpu'] != 0 && $namespace['quota_hard_cpu'] % 1000 == 0) {
                  $namespace['quota_hard_cpu'] /= 1000;
                } else {
                  $namespace['quota_hard_cpu'] .= "m";
                }
                if($namespace['quota_hard_memory'] != 0 && $namespace['quota_hard_memory'] % 1024 == 0){
                  $namespace['quota_hard_memory'] /= 1024;
                  $namespace['quota_hard_memory'] .= "Gi";
                } else {
                  $namespace['quota_hard_memory'] .= "Mi";
                }
                $value["$namespace[name]"] = $namespace;
              }
              $content['override_id'] = $override['id'];
              // $content['value'] = $value;
              $content['value'] = json_encode($value, JSON_UNESCAPED_SLASHES)."\n";
              $output = $this->foreman->write_smartclass($config['foreman_var_namespaces'], $content);
            }
          }
        }
        elseif(isset($smartclass['error'])){
          $output = "ERROR<BR/>Cannot connect to Foreman.<BR/>Reason: ";
          $output .= $smartclass['error']['message'];
        }
        else {
          $output = "Error. Cannot connect to Foreman.";
          $output .= $smartclass;
          $output.= $config;
        }
      }
      if($object == "volumes")
      {
        $this->load->model('Volume_model');
        $this->load->model('Knamespace_model');
        $smartclass = $this->foreman->read_smartclass($config['foreman_var_volumes']);

        if(isset($smartclass['id']))
        {
          $content = array();
          $value = array();
          $output = "";
          foreach($smartclass['override_values'] as $override)
          {
            if($override['match'] == "fqdn=".$config['kubernetes_master_fqdn'])
            {
              $volumes = $this->Volume_model->get_all_volumes();
              foreach ($volumes as $volume) {
                // echo "hehe".$volume."ohoh";
                $id= [ 'id' => "$volume[id]"];
                if ($this->Knamespace_model->get_knamespace($volume['namespace_id']) != null) {
                  $volume['namespace']= $this->Knamespace_model->get_knamespace($volume['namespace_id'])['name'];
                }
                else {
                  $output .="Error Namespace not found for namespace_id: ".$volume['namespace_id']."<br/>\n";
                  var_dump($volume);
                  continue;
                }

                $volume['subdir'] = $volume['path'];
                $volume['ReclaimPolicy'] = $volume['reclaimpolicy'];
                $volume['size'] .= "Gi";
                unset($volume['path']);
                unset($volume['id']);
                unset($volume['namespace_id']);
                unset($volume['reclaimpolicy']);
                $value["$volume[name]"] = $volume;
              }
              $content['override_id'] = $override['id'];
              // $content['value'] = $value;
              $content['value'] = json_encode($value, JSON_UNESCAPED_SLASHES)."\n";

              $output .= $this->foreman->write_smartclass($config['foreman_var_volumes'], $content);
            }
          }
        }
        elseif(isset($smartclass['error'])){
          $output = "ERROR<BR/>Cannot connect to Foreman.<BR/>Reason: ";
          $output .= $smartclass['error']['message'];
        }
        else {
          $output = "Error. Cannot connect to Foreman.";
          $output .= $smartclass;
          $output.= $config;
        }
      }
      if($object == "policy")
      {
        $this->load->model('Rule_model');
        $this->load->model('User_model');
        $this->load->model('Knamespace_model');
        $smartclass = $this->foreman->read_smartclass($config['foreman_var_policy']);

        if(isset($smartclass['id']))
        {
          $content = array();
          $value = array();
          foreach($smartclass['override_values'] as $override)
          {
            if($override['match'] == "fqdn=".$config['kubernetes_master_fqdn'])
            {
              $rules = $this->Rule_model->get_all_rules();
              $users = $this->User_model->get_all_users();
              $namespaces = $this->Knamespace_model->get_all_knamespaces();
              $config = $this->Config_model->get_default_config();
              $output = $config['policy_custom']."\n";

              foreach ($rules as $rule) {
                $id= [ 'id' => "$rule[id]"];
                $json_array["apiversion"] = "abac.authorization.kubernetes.io/v1beta1";
             		$json_array["kind"] = "Policy";
             		$json_array["spec"]['user'] = $this->User_model->get_user($rule['user_id'])['name'];
             		$json_array["spec"]['apiGroup'] ="*";
             		$json_array["spec"]['namespace'] = $this->Knamespace_model->get_knamespace($rule['namespace_id'])['name'];
             		$json_array["spec"]['resource'] ="*";
             		$json_array["spec"]['nonResourcePath'] ="*";
             		if(isset($rule['is_admin']) && $rule['is_admin'] == "1") {
             			$json_array["spec"]['readonly'] =false;
             		} else {
             			$json_array["spec"]['readonly'] =true;
             		}
             		$output .= json_encode($json_array, JSON_UNESCAPED_SLASHES)."\n";
              }
              $content['override_id'] = $override['id'];
              $content['value'] = $output;
              $output = $this->foreman->write_smartclass($config['foreman_var_policy'], $content);
            }
          }
        }
        elseif(isset($smartclass['error'])){
          $output  = "ERROR<BR/>Cannot connect to Foreman.<BR/>Reason: ";
          $output .= $smartclass['error']['message'];
        }
        else {
          $output  = "Error. Cannot connect to Foreman.";
          $output .= $smartclass;
          $output .= $config;
        }
      }

      $data['config']['output'] = $output;
      $data['_view'] = 'config/sync';
      $this->load->view('layouts/main',$data);

    }
}
