<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Config</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('config/add'); ?>" class="btn btn-success btn-sm">Add</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
            <th>Name</th>
            <th>Foreman Url</th>
						<th>Kubernetes Master Fqdn</th>
						<th>Foreman Login</th>
						<!-- <th>Foreman Password</th> -->
						<th>Foreman Var Namespaces</th>
						<th>Foreman Var Volumes</th>
            <th>Foreman Var Policy</th>
            <!-- <th>ID</th> -->
						<!-- <th>Policy Custom</th> -->
						<th>Actions</th>
                    </tr>
                    <?php foreach($config as $c){ ?>
                    <tr>
            <td><?php echo $c['name']; ?></td>
            <td><?php echo $c['foreman_url']; ?></td>
						<td><?php echo $c['kubernetes_master_fqdn']; ?></td>
						<td><?php echo $c['foreman_login']; ?></td>
						<!-- <td><!--?php echo $c['foreman_password']; ?></td> -->
						<td><?php echo $c['foreman_var_namespaces']; ?></td>
						<td><?php echo $c['foreman_var_volumes']; ?></td>
            <td><?php echo $c['foreman_var_policy']; ?></td>
            <!-- <td><!--?php echo $c['id']; ?></td> -->
						<!-- <td><!--?php echo $c['policy_custom']; ?></td> -->
						<td>
                            <a href="<?php echo site_url('config/edit/'.$c['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
                            <a href="<?php echo site_url('config/remove/'.$c['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                            <a href="<?php echo site_url('config/default/'.$c['id']); ?>" <?php
                                          if($c['is_default'] == 1){
                                            echo 'class="btn btn-success btn-xs"><span class="fa fa-power-off"></span> Default</a>';
                                          }
                                          else {
                                            echo 'class="btn btn-xs"><span class="fa"></span>Set default</a>';
                                          } ?>
            </td>
            </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>
