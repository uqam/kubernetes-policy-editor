<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Config Edit</h3>
            </div>
			<?php echo form_open('config/edit/'.$config['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
          <div class="col-md-6">
            <label for="name" class="control-label">Name</label>
            <div class="form-group">
              <input type="text" name="name" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $config['name']); ?>" class="form-control" id="name" readonly/>
              <span class="text-danger"><?php echo form_error('name');?></span>
            </div>
          </div>
          <div class="col-md-6">
            <label for="foreman_url" class="control-label"><span class="text-danger">*</span>Foreman Url</label>
            <div class="form-group">
              <input type="text" name="foreman_url" value="<?php echo ($this->input->post('foreman_url') ? $this->input->post('foreman_url') : $config['foreman_url']); ?>" class="form-control" id="foreman_url" />
              <span class="text-danger"><?php echo form_error('foreman_url');?></span>
            </div>
          </div>
					<div class="col-md-6">
						<label for="kubernetes_master_fqdn" class="control-label">Kubernetes Master Fqdn</label>
						<div class="form-group">
							<input type="text" name="kubernetes_master_fqdn" value="<?php echo ($this->input->post('kubernetes_master_fqdn') ? $this->input->post('kubernetes_master_fqdn') : $config['kubernetes_master_fqdn']); ?>" class="form-control" id="kubernetes_master_fqdn" />
							<span class="text-danger"><?php echo form_error('kubernetes_master_fqdn');?></span>
						</div>
					</div>
          <div class="col-md-6">
            <label for="is_default" class="control-label"><span class="text-danger">*</span>Is Default</label>
            <div class="form-group">
              <input type="checkbox" name="is_default" value="1" <?php echo ($config['is_default']==1 ? 'checked="checked"' : ''); ?> id='is_default' />
              <span class="text-danger"><?php echo form_error('is_default');?></span>
            </div>
          </div>
					<div class="col-md-6">
						<label for="foreman_login" class="control-label"><span class="text-danger">*</span>Foreman Login</label>
						<div class="form-group">
							<input type="text" name="foreman_login" value="<?php echo ($this->input->post('foreman_login') ? $this->input->post('foreman_login') : $config['foreman_login']); ?>" class="form-control" id="foreman_login" />
							<span class="text-danger"><?php echo form_error('foreman_login');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="foreman_password" class="control-label"><span class="text-danger">*</span>Foreman Password</label>
						<div class="form-group">
							<input type="text" name="foreman_password" value="<?php echo ($this->input->post('foreman_password') ? $this->input->post('foreman_password') : $config['foreman_password']); ?>" class="form-control" id="foreman_password" />
							<span class="text-danger"><?php echo form_error('foreman_password');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="foreman_var_namespaces" class="control-label"><span class="text-danger">*</span>Foreman Var Namespaces</label>
						<div class="form-group">
							<input type="text" name="foreman_var_namespaces" value="<?php echo ($this->input->post('foreman_var_namespaces') ? $this->input->post('foreman_var_namespaces') : $config['foreman_var_namespaces']); ?>" class="form-control" id="foreman_var_namespaces" />
							<span class="text-danger"><?php echo form_error('foreman_var_namespaces');?></span>
						</div>
					</div>
          <div class="col-md-6">
						<label for="foreman_var_policy" class="control-label"><span class="text-danger">*</span>Foreman Var Policy</label>
						<div class="form-group">
              <input type="text" name="foreman_var_policy" value="<?php echo ($this->input->post('foreman_var_policy') ? $this->input->post('foreman_var_policy') : $config['foreman_var_policy']); ?>" class="form-control" id="foreman_var_policy" />
							<span class="text-danger"><?php echo form_error('foreman_var_policy');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="foreman_var_volumes" class="control-label"><span class="text-danger">*</span>Foreman Var Volumes</label>
						<div class="form-group">
							<input type="text" name="foreman_var_volumes" value="<?php echo ($this->input->post('foreman_var_volumes') ? $this->input->post('foreman_var_volumes') : $config['foreman_var_volumes']); ?>" class="form-control" id="foreman_var_volumes" />
							<span class="text-danger"><?php echo form_error('foreman_var_volumes');?></span>
						</div>
					</div>
				</div>
        <div class="row-fluid clearfix">
          <label for="policy_custom" class="control-label">Policy Custom</label>
          <div class="form-group">
            <textarea name="policy_custom" class="form-control" id="policy_custom" style="height: 300px;"><?php echo ($this->input->post('policy_custom') ? $this->input->post('policy_custom') : $config['policy_custom']); ?></textarea>
          </div>
        </div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>
			<?php echo form_close(); ?>
		</div>
    </div>
</div>
