<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header">
          <h3 class="box-title">Rules import</h3>
      </div>
      <div class="box-body">
      <?php
        if(isset($output)) {
          echo $output;
          echo "<div style='margin-top:20px;' class='btn btn-default btn-sm'><a href=/rule >Back</a></div>";
        } else {
          echo "<div class='form-group'>";
            echo "<div class='col-md-4'>";
              echo "<div class='box box-success'>";
                echo "<div class='box-header with-border'>";
                  echo "<h3 class=\"box-title\">JSON</h3>";
                  echo form_open('rule/import');
                  echo form_textarea("data");
                  echo "<div class=\"box-header with-border\">";
                  echo "<h3 class=\"box-title\">Options</h3>";
                  echo "</div>";
                  $data_checkbox = array(
                    'name' => "autocreate",
                    'value'=> TRUE,
                  );
                  echo form_checkbox($data_checkbox);
                  echo form_label("Auto create missing namespaces and users with default parameters");

                  echo "<div class=\"box-header with-border\">";
                  echo "<h3 class=\"box-title\"></h3>";
                  echo "</div>";
                  echo form_submit('import_submit', 'Go!', "class=\"btn btn-success btn-sm\"");
                  echo form_close();
                echo "</div>";
              echo "</div>";
            echo "</div>";
          echo "</div>";
        }
      ?>
      </div>
    </div>
  </div>
</div>
