<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Rules admin panel</h3>
            </div>
            <?php
              if(isset($selected_user_id)) echo form_open('rule/add/'.$selected_user_id);
              echo form_open('rule/add');
            ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="user_id" class="control-label"><span class="text-danger">*</span>User</label>
						<div class="form-group">
							<select name="user_id" id="select_user" class="form-control" onchange="window.location.href='/rule/add/'+document.getElementById('select_user').value;">
								<option value="">select user</option>
								<?php
								foreach($all_users as $user)
								{
									// $selected = ($user['id'] == $this->input->post('user_id')) ? ' selected="selected"' : "";
                  $selected = ($user['id'] == $selected_user_id) ? ' selected="selected"' : "";

									echo '<option value="'.$user['id'].'" '.$selected.'>'.$user['name'].'</option>';
								}
								?>
							</select>
							<span class="text-danger"><?php echo form_error('user_id');?></span>
						</div>
            <label for="comment" class="control-label">Comment</label>
            <div class="form-group">
              <input type="text" name="comment" value="<?php echo $this->input->post('comment'); ?>" class="form-control" id="comment" />
            </div>
					</div>
					<div class="col-md-6">
						<label for="namespace_id" class="control-label"><span class="text-danger">*</span>Namespace</label>
						<div class="form-group">
							<!-- <select name="namespace_id" class="form-control"> -->
								<!-- <option value="">select namespace</option> -->
                <table class='table table-hover'>
                  <!-- <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Namespaces<span class="caret"></span></button> -->
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Read/Write</th>
                      <th>Read Only</th>
                    </tr>
                  </thead>
                  <tbody>
								<?php
                $read_namespaces_id = array();
                $write_namespaces_id = array();
                if(isset($user_rules)) {
                  foreach ($user_rules as $user_rule) {
                    if ($user_rule['is_admin']) array_push($write_namespaces_id, $user_rule['namespace_id']);
                    else array_push($read_namespaces_id, $user_rule['namespace_id']);
                  }
                }
								foreach($all_knamespaces as $knamespace)
								{
                  echo "<tr>";
                  echo "<td>";
                  echo $knamespace['name'];
                  echo "</td>";
                  echo "<td>";
                  $values = array(
                    'id'      => $knamespace['id'],
                    'name'    => $knamespace['id'],
                    'value'   => 1,
                    'checked' => ( in_array($knamespace['id'], $write_namespaces_id) ) ? TRUE : FALSE,
                  );
                  echo form_radio($values);
                  echo "</td>";
                  echo "<td>";
                  $values = array(
                    'id'      => $knamespace['id'],
                    'name'    => $knamespace['id'],
                    'value'   => 0,
                    'checked' => ( in_array($knamespace['id'], $read_namespaces_id) ) ? TRUE : FALSE,
                  );
                  echo form_radio($values);
                  echo "</td>";
                  echo "</tr>\n";

									// $selected = ($knamespace['id'] == $this->input->post('namespace_id')) ? ' selected="selected"' : "";

									// echo '<option value="'.$knamespace['id'].'" '.$selected.'>'.$knamespace['name'].'</option>';
								}
								?>
                <!-- </ul> -->
              </table>
							<!-- </select> -->
							<!-- <span class="text-danger"><!--?php echo form_error('namespace_id');?></span> -->
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
              <a href="/rule/index" class="btn btn-default">Back</a>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
