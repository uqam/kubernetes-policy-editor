<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Rules</h3>
                <div class="box-tools col-md-3">
                  <div class='form-group'>
                    <?php
                    echo form_open('rule/index',array("method=get"));
                    echo form_label("Search rules:","",array("style"=>'margin-right:10px'));
                    echo form_input("search");
                    // echo form_submit('search', 'Search', "class='btn btn-default'");
                    echo form_close();
                    ?>
                  </div>
                </div>
            	<div class="box-tools col-md-0">
                    <a href="<?php echo site_url('rule/add'); ?>" class="btn btn-success btn-sm">Add</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
            <?php
            # Display of sort buttons in column headers:
            if(isset($params['sort']) && $params['sort'] == "users.name") {
              if(isset($params['order']) && $params['order'] == 'asc') {
                $params['order'] = 'desc';
                echo "<th><a class='fa fa-sort-desc' href='/rule/index?&".http_build_query($params)."'>User</a></th>";
              }
              else {
                $params['order'] = 'asc';
                echo "<th><a class='fa fa-sort-asc' href='/rule/index?&".http_build_query($params)."'>User</a></th>";
              }
            } else {
              # If sort and order attribute are already set but not with users.name
              # We rebuild the query string without adding extra sort and order attributes
              $other_params = $params;
              if (isset($params['sort']) && isset($params['order'])) unset($other_params['sort'], $other_params['order']);
              echo "<th><a class='fa fa-sort' href='/rule/index?&sort=users.name&order=asc&".http_build_query($other_params)."'>User</a></th>";
            }
            if(isset($params['sort']) && $params['sort'] == "namespaces.name") {
              $params['sort'] = 'namespaces.name';

              if(isset($params['order']) && $params['order'] == 'asc') {
                $params['order'] = 'desc';
                echo "<th><a class='fa fa-sort-desc' href='/rule/index?&".http_build_query($params)."'>Namespace</a></th>";
              }
              else {
                $params['order'] = 'asc';
                echo "<th><a class='fa fa-sort-asc' href='/rule/index?&".http_build_query($params)."'>Namespace</a></th>"; }
            } else {
              # If sort and order attribute are already set but not with namespaces.name
              # We rebuild the query string without adding extra sort and order attributes
              $other_params = $params;
              if (isset($params['sort']) && isset($params['order'])) unset($other_params['sort'], $other_params['order']);
              echo "<th><a class='fa fa-sort' href='/rule/index?&sort=namespaces.name&order=asc&".http_build_query($other_params)."'>Namespace</a></th>";
            }
						echo "<th>R/W</th>";
						echo "<th>Comment</th>";
						echo "<th>Actions</th>";
            ?>
                    </tr>
                    <?php
                    if(empty($rules) && isset($params['search'])) {
                      echo "No result for <b>".$params['search']."</b><br/>";
                    } elseif ($rules != ""){
                    foreach($rules as $r){ ?>
                    <tr>
						<td><?php foreach($all_users as $us) {  if( $r['user_id'] == $us['id']) { $user_id = $us['id']; print $us['name']; } } ?></td>
						<td><?php foreach($all_knamespaces as $ns) {  if( $r['namespace_id'] == $ns['id']) { print $ns['name']; } } ?></td>
						<td><?php if($r['is_admin'] == 0) { print '<i class="fa fa-square-o" aria-hidden="true"></i>'; } else {print '<i class="fa fa-check-square-o" aria-hidden="true"></i>';} ?></td>
						<td><?php echo $r['comment']; ?></td>
						<td>
                            <a href="<?php echo site_url('rule/add/'.$user_id); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
                            <a href="<?php echo site_url('rule/remove/'.$r['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php }
                    } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
        <a href="/config/sync/policy" class="btn btn-success btn"><span class="fa fa-refresh"></span> Sync with Foreman</a>
	      <a href="/rule/json" class="btn btn-warning btn"><span class="fa fa-wrench"></span> Export Policy.json for Kubernetes API Server (ABAC)</a>
        <a href="/rule/import" class="btn btn-danger btn"><span class="fa fa-arrow-circle-up"></span> Import JSON</a>
    </div>
</div>
