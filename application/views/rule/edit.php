<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Rule Edit</h3>
            </div>
			<?php echo form_open('rule/edit/'.$rule['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="user_id" class="control-label"><span class="text-danger">*</span>User</label>
						<div class="form-group">
							<select name="user_id" class="form-control">
								<option value="">select user</option>
								<?php 
								foreach($all_users as $user)
								{
									$selected = ($user['id'] == $rule['user_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$user['id'].'" '.$selected.'>'.$user['name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('user_id');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="namespace_id" class="control-label"><span class="text-danger">*</span>Namespace</label>
						<div class="form-group">
							<select name="namespace_id" class="form-control">
								<option value="">select namespace</option>
								<?php 
								foreach($all_knamespaces as $knamespace)
								{
									$selected = ($knamespace['id'] == $rule['namespace_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$knamespace['id'].'" '.$selected.'>'.$knamespace['name'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('namespace_id');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="comment" class="control-label">Comment</label>
						<div class="form-group">
							<input type="text" name="comment" value="<?php echo ($this->input->post('comment') ? $this->input->post('comment') : $rule['comment']); ?>" class="form-control" id="comment" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="is_admin" value="1" <?php echo ($rule['is_admin']==1 ? 'checked="checked"' : ''); ?> id='is_admin' />
							<label for="is_admin" class="control-label"><span class="text-danger">*</span> R/W</label>
							<span class="text-danger"><?php echo form_error('is_admin');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>
