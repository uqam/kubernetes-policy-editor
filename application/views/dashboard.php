<div class="col-md-4">
          <!-- Info Boxes Style 2 -->
          <h1>CPU</h1>
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="ion ion-flame"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Cpu Requests</span>
              <span class="info-box-number"><?php echo $namespace['total_cpu_requests'] ?> millicores - <?php echo round($namespace['total_cpu_requests']/400) ?>%</span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php echo $namespace['total_cpu_requests']/400 ?>%"></div>
              </div>
              <span class="progress-description">
                    Out of 20 cpu (20 000 millicores)
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-bonfire"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Cpu Limits</span>
              <span class="info-box-number"><?php echo $namespace['total_cpu_limits'] ?> millicores - <?php echo round($namespace['total_cpu_limits']/400) ?>%</span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php echo $namespace['total_cpu_limits']/400 ?>%"></div>
              </div>
              <span class="progress-description">
                    Out of 40 cpu (40 000 millicores)
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <h1>Memory</h1>
          <!-- /.info-box -->
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-microchip"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Memory Requests</span>
              <span class="info-box-number"><?php echo $namespace['total_mem_requests'] ?> Mi - <?php echo round($namespace['total_mem_requests']/800) ?>%</span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php echo $namespace['total_mem_requests']/800 ?>%"></div>
              </div>
              <span class="progress-description">
                    Out of 80 Gi ( 5 x 16Gi )
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-microchip"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Memory Limits</span>
              <span class="info-box-number"><?php echo $namespace['total_mem_limits'] ?> Mi - <?php echo round($namespace['total_mem_limits']/800) ?>%</span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php echo $namespace['total_mem_limits']/800 ?>%"></div>
              </div>
              <span class="progress-description">
                    Out of 80 Gi ( 5 x 16Gi )
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
      <div class="col-md-4">
          <!-- /.info-box -->
          <h1>Storage</h1>
          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="fa fa-file-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total allocated NFS storage</span>
              <span class="info-box-number"><?php echo $volume['total_size'] ?> Gi - <?php echo round($volume['total_size']/124*100) ?>%</span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php echo $volume['total_size']/124*100 ?>%"></div>
              </div>
              <span class="progress-description">
                    Out of 80 Gi ( 5 x 16Gi )
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <h1>Statistics</h1>
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Users</span>
              <span class="info-box-number"><?php echo $stats['users_count'] ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-cube"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Namespaces</span>
              <span class="info-box-number"><?php echo $stats['namespaces_count'] ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-database"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Volumes</span>
              <span class="info-box-number"><?php echo $stats['volumes_count'] ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
