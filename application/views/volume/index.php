<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Volumes</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('volume/add'); ?>" class="btn btn-success btn-sm">Add</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<!-- <th>ID</th> -->
            <th>Name</th>
						<th>Size</th>
						<!-- <th>Namespace Id</th> -->
						<th>Mode</th>
						<th>Reclaimpolicy</th>
						<th>Path</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($volumes as $v){ ?>
                    <tr>
						<!-- <td><!--?php echo $v['id']; ?></td> -->
            <td><?php echo $v['name']; ?></td>
						<td><?php echo $v['size']; ?> Gi</td>
						<!-- <td><!--?php echo $v['namespace_id']; ?></td> -->
						<td><?php echo $v['mode']; ?></td>
						<td><?php echo $v['reclaimpolicy']; ?></td>
						<td><?php echo $v['path']; ?></td>
						<td>
                            <!-- <a href="<?php echo site_url('volume/edit/'.$v['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> -->
                            <a href="<?php echo site_url('volume/remove/'.$v['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
        <a href="/config/sync/volumes" class="btn btn-success btn"><span class="fa fa-refresh"></span> Sync with Foreman</a>
        <a href="/volume/yaml" class="btn btn-warning btn"><span class="fa fa-wrench"></span> Export YAML for Kubernetes Puppet Module</a>
        <a href="/volume/import" class="btn btn-danger btn"><span class="fa fa-arrow-circle-up"></span> Import YAML</a>
    </div>
</div>
