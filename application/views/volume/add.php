<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Volume Add</h3>
            </div>
            <?php echo form_open('volume/add', "onchange=\"document.getElementById('name').value = document.getElementById('namespace_id').options[document.getElementById('namespace_id').selectedIndex].text+'-'+document.getElementById('path').value;\""); ?>
          	<div class="box-body">
          		<div class="row clearfix">
                <div class="col-md-6">
      						<label for="name" class="control-label"><span class="text-danger">*</span>Name</label>
      						<div class="form-group">
      							<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" readonly/>
      							<span class="text-danger"><?php echo form_error('name');?></span>
      						</div>
      					</div>
                <div class="col-md-6">
      						<label for="namespace_id" class="control-label"><span class="text-danger">*</span>Knamespace</label>
      						<div class="form-group">
      							<select name="namespace_id" class="form-control" id="namespace_id">
      								<option value="">select knamespace</option>
      								<?php
      								foreach($all_knamespaces as $knamespace)
      								{
      									$selected = ($knamespace['id'] == $this->input->post('namespace_id')) ? ' selected="selected"' : "";
      									echo '<option value="'.$knamespace['id'].'" '.$selected.'>'.$knamespace['name'].'</option>';
      								}
      								?>
      							</select>
      							<span class="text-danger"><?php echo form_error('namespace_id');?></span>
      						</div>
      					</div>
      					<div class="col-md-6">
      						<label for="size" class="control-label"><span class="text-danger">*</span>Size</label>
      						<div class="form-group">
      							<select name="size" class="form-control">
      								<option value="">select</option>
      								<?php
      								$size_values = array(
      									'1'=>'1Gi',
      									'2'=>'2Gi',
      									'5'=>'5Gi',
      									'10'=>'10Gi',
      									'20'=>'20Gi',
      									'50'=>'50Gi',
      									'100'=>'100Gi',
      									'200'=>'200Gi',
      									'500'=>'500Gi',
      								);

      								foreach($size_values as $value => $display_text)
      								{
      									$selected = ($value == $this->input->post('size')) ? ' selected="selected"' : "";

      									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
      								}
      								?>
      							</select>
      							<span class="text-danger"><?php echo form_error('size');?></span>
      						</div>
      					</div>
					<div class="col-md-6">
						<label for="mode" class="control-label">Mode</label>
						<div class="form-group">
							<select name="mode" class="form-control">
								<option value="">select</option>
								<?php
								$mode_values = array(
									'ReadWriteOnce'=>'ReadWriteOnce',
									'ReadWriteMany'=>'ReadWriteMany',
									'ReadOnlyMany'=>'ReadOnlyMany',
								);

								foreach($mode_values as $value => $display_text)
								{
									$selected = ($value == $this->input->post('mode')) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="reclaimpolicy" class="control-label">Reclaimpolicy</label>
						<div class="form-group">
							<select name="reclaimpolicy" class="form-control">
								<option value="">select</option>
								<?php
								$reclaimpolicy_values = array(
									'Retain'=>'Retain',
									'Recycle'=>'Recycle',
									'Delete'=>'Delete',
								);

								foreach($reclaimpolicy_values as $value => $display_text)
								{
									$selected = ($value == $this->input->post('reclaimpolicy')) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="path" class="control-label"><span class="text-danger">*</span>Path</label>
						<div class="form-group">
							<input type="text" name="path" value="<?php echo $this->input->post('path'); ?>" class="form-control" id="path" />
							<span class="text-danger"><?php echo form_error('path');?></span>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
