<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Knamespace Edit</h3>
            </div>
			<?php echo form_open('knamespace/edit/'.$knamespace['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="name" class="control-label"><span class="text-danger">*</span>Name</label>
						<div class="form-group">
							<input type="text" name="name" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $knamespace['name']); ?>" class="form-control" id="name" readonly/>
							<span class="text-danger"><?php echo form_error('name');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="quota_hard_pods" class="control-label"><span class="text-danger">*</span>&sum; Pods</label>
						<div class="form-group">
							<input type="text" name="quota_hard_pods" value="<?php echo ($this->input->post('quota_hard_pods') ? $this->input->post('quota_hard_pods') : $knamespace['quota_hard_pods']); ?>" class="form-control" id="quota_hard_pods" />
							<span class="text-danger"><?php echo form_error('quota_hard_pods');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="quota_hard_limits_cpu" class="control-label"><span class="text-danger">*</span>&sum; Limits Cpu - <I>unit: m</I></label>
						<div class="form-group">
							<input type="text" name="quota_hard_limits_cpu" value="<?php echo ($this->input->post('quota_hard_limits_cpu') ? $this->input->post('quota_hard_limits_cpu') : $knamespace['quota_hard_limits_cpu']); ?>" class="form-control" id="quota_hard_limits_cpu" />
							<span class="text-danger"><?php echo form_error('quota_hard_limits_cpu');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="quota_hard_limits_memory" class="control-label"><span class="text-danger">*</span>&sum; Limits Memory - <I>unit: Mi</I></label>
						<div class="form-group">
							<input type="text" name="quota_hard_limits_memory" value="<?php echo ($this->input->post('quota_hard_limits_memory') ? $this->input->post('quota_hard_limits_memory') : $knamespace['quota_hard_limits_memory']); ?>" class="form-control" id="quota_hard_limits_memory" />
							<span class="text-danger"><?php echo form_error('quota_hard_limits_memory');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="quota_hard_cpu" class="control-label"><span class="text-danger">*</span>&sum; Requests Cpu - <I>unit: m</I></label>
						<div class="form-group">
							<input type="text" name="quota_hard_cpu" value="<?php echo ($this->input->post('quota_hard_cpu') ? $this->input->post('quota_hard_cpu') : $knamespace['quota_hard_cpu']); ?>" class="form-control" id="quota_hard_cpu" />
							<span class="text-danger"><?php echo form_error('quota_hard_cpu');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="quota_hard_memory" class="control-label"><span class="text-danger">*</span>&sum; Requests Memory - <I>unit: Mi</I></label>
						<div class="form-group">
							<input type="text" name="quota_hard_memory" value="<?php echo ($this->input->post('quota_hard_memory') ? $this->input->post('quota_hard_memory') : $knamespace['quota_hard_memory']); ?>" class="form-control" id="quota_hard_memory" />
							<span class="text-danger"><?php echo form_error('quota_hard_memory');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>
			<?php echo form_close(); ?>
		</div>
    </div>
</div>
