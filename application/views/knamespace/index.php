<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Namespaces</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('knamespace/add'); ?>" class="btn btn-success btn-sm">Add</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Name</th>
						<th>&sum; Pods</th>
						<th>&sum; Limits Cpu</th>
						<th>&sum; Limits Memory</th>
						<th>&sum; Requests Cpu</th>
						<th>&sum; Requests Memory</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($knamespaces as $n){ ?>
                    <tr>
						<td><?php echo $n['name']; ?></td>
						<td><?php echo $n['quota_hard_pods']; ?></td>
						<td><?php echo $n['quota_hard_limits_cpu']; ?>m</td>
						<td><?php echo $n['quota_hard_limits_memory']; ?>Mi</td>
						<td><?php echo $n['quota_hard_cpu']; ?>m</td>
						<td><?php echo $n['quota_hard_memory']; ?>Mi</td>
						<td>
                            <a href="<?php echo site_url('knamespace/edit/'.$n['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
                            <a href="<?php echo site_url('knamespace/remove/'.$n['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
         <a href="/config/sync/namespaces" class="btn btn-success btn"><span class="fa fa-refresh"></span> Sync with Foreman</a>
         <a href="/knamespace/yaml" class="btn btn-warning btn"><span class="fa fa-wrench"></span> Export YAML for Kubernetes Puppet Module</a>
         <a href="/knamespace/import" class="btn btn-danger btn"><span class="fa fa-arrow-circle-up"></span> Import YAML</a>
    </div>
</div>
