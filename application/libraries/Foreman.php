<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Foreman Php API
 *
 * @author     Craig Parker ab5w
 * @license    GPL License
 * @copyright  Copyright (C) 2015 Craig Parker <craig@ab5w.com>
 */

class Foreman {

    private $fmurl;
    private $fmusername;
    private $fmpassword;

    public function __construct($config = array()) {
        $this->fmurl = $config['url'];
        $this->fmusername = $config['username'];
        $this->fmpassword = $config['password'];
    }

    public function query($endpoint = "dashboard") {

        $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, "$this->fmusername:$this->fmpassword");
            curl_setopt($ch, CURLOPT_URL,$this->fmurl . "/api/" . $endpoint);

        $output = curl_exec($ch);
        return $output;
    }

    public function hostlist() {

        $totalhosts = $this->query();
        $totalhosts = json_decode($totalhosts,true);
        $totalhosts = $totalhosts['total_hosts'];

        $hosts = "hosts?per_page=" . $totalhosts;
        $hosts = $this->query($hosts);
        $hosts = json_decode($hosts,true);

        foreach ($hosts['results'] as $host) {
            $hostnames[] = $host['name'];
        }

        $hostnames = json_encode($hostnames);
        return $hostnames;
    }

    public function delete_host($host) {

        $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, "$this->fmusername:$this->fmpassword");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_URL,$this->fmurl . "/api/hosts/" . $host);

        $output = curl_exec($ch);
        return $output;
    }

    public function read_smartclass($id) {

        $smart_class_parameter = $this->query("v2/smart_class_parameters/".$id);
        $smart_class_parameter = json_decode($smart_class_parameter,true);

        return $smart_class_parameter;
    }

    public function write_smartclass($id, $data) {

      // var_dump(json_encode($data));
      $ch = curl_init();
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, false);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
          curl_setopt($ch, CURLOPT_USERPWD, "$this->fmusername:$this->fmpassword");
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
          curl_setopt($ch, CURLOPT_URL,$this->fmurl . "/api/smart_class_parameters/" . $id . "/override_values/".$data['override_id']);
          $value['value']=$data['value'];

          // var_dump(json_encode($value['value']));
          // var_dump($value);
          curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($value));
      $output = curl_exec($ch);
      return $output;
    }
}
