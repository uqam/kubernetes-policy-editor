Kubernetes-Policy-Editor
==========================
Minimal dashboard to manage Kubernetes resources (including namespaces, quota/limits and policy.json (RBAC).
Work in progress.

![Settings Window](https://bytebucket.org/uqam/kubernetes-policy-editor/raw/78913cc4db520f267d106f6afeab16e9faf82637/misc/1.png "Screenshot")
![Settings Window](https://bytebucket.org/uqam/kubernetes-policy-editor/raw/78913cc4db520f267d106f6afeab16e9faf82637/misc/2.png "Screenshot")
![Settings Window](https://bytebucket.org/uqam/kubernetes-policy-editor/raw/78913cc4db520f267d106f6afeab16e9faf82637/misc/3.png "Screenshot")

Dependencies: php-mysql php-mbstrings php-curl
