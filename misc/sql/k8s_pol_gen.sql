-- Generated by CRUDigniter v2.3 Beta 
-- www.crudigniter.com

-- Generation Time: Jul 05, 2017 01:03 AM

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
`id` int(11) NOT NULL,
`foreman_login` varchar(45) ,
`foreman_password` varchar(45) ,
`foreman_url` varchar(45) ,
`foreman_var_namespaces` varchar(45) ,
`foreman_var_volumes` varchar(45) ,
`name` varchar(45) ,
`kubernetes_master_fqdn` VARCHAR(255) ,
`policy_custom` TEXT ,
`is_default` BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `config`
--

ALTER TABLE `config`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `config`
--

ALTER TABLE `config`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `namespaces`
--

DROP TABLE IF EXISTS `namespaces`;
CREATE TABLE `namespaces` (
`id` int(11) NOT NULL,
`name` varchar(45) NOT NULL,
`quota_hard_pods` int(255) NOT NULL,
`quota_hard_limits_cpu` int(255) NOT NULL,
`quota_hard_limits_memory` int(255) NOT NULL,
`quota_hard_cpu` int(255) NOT NULL,
`quota_hard_memory` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `namespaces`
--

ALTER TABLE `namespaces`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `namespaces`
--

ALTER TABLE `namespaces`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

DROP TABLE IF EXISTS `rules`;
CREATE TABLE `rules` (
`id` int(255) NOT NULL,
`comment` VARCHAR(255) ,
`user_id` INT(255) ,
`namespace_id` INT(255) ,
`is_admin` BOOLEAN 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `rules`
--

ALTER TABLE `rules`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `rules`
--

ALTER TABLE `rules`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
`id` int(11) NOT NULL,
`name` varchar(45) ,
`email` varchar(45) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `users`
--

ALTER TABLE `users`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `users`
--

ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `volumes`
--

DROP TABLE IF EXISTS `volumes`;
CREATE TABLE `volumes` (
`id` INT(255) ,
`name` VARCHAR(255) ,
`size` INT(255) ,
`namespace_id` INT(255) ,
`path` VARCHAR(255) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `volumes`
--

ALTER TABLE `volumes`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `volumes`
--

ALTER TABLE `volumes`
MODIFY `id` INT(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
